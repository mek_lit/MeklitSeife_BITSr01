let button = document.querySelector(".theme-button");
        // Define the light and dark colors
        let lightBg = "white";
        let lightSection = "white";
        let darkBg = "black";
        let darkSection = "rgb(232, 253, 250)";
        // Define a variable to track the theme
        let isLight = true;
        // Add a click event listener to the button
        button.addEventListener("click", function() {
        // If the theme is light, change it to dark
        if (isLight) {
            // Set the variables to the dark colors
            document.documentElement.style.setProperty("--bg-color", darkBg);
            document.documentElement.style.setProperty("--section-color", darkSection);
            // Change the button text
            button.textContent = "Toggle Theme";
            // Set the theme variable to false
            isLight = false;
        } else {
            // If the theme is dark, change it to light
            // Set the variables to the light colors
            document.documentElement.style.setProperty("--bg-color", lightBg);
            document.documentElement.style.setProperty("--section-color", lightSection);
                    // Change the button text
            button.textContent = "Toggle Theme";
                    // Set the theme variable to true
            isLight = true;
            }
        });
        